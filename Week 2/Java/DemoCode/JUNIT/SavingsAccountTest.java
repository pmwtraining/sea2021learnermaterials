package qa;

import static org.junit.jupiter.api.Assertions.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SavingsAccountTest {
    SavingsAccount account;
    double delta=0.001;

    @BeforeEach
    void setUp() throws Exception {
        account= new SavingsAccount();
        account.setName("Peter);
        account.setBalance(5000);
        account.setAccountNumber(1234);
    }

    @AfterEach
    void tearDown() throws Exception {
        account=null;
    }

    @Test
    void depositTest() {
        account.deposit(500);
        assertEquals(account.getBalance(),5500,delta,"Deposit Test");
    }

    @Test
    void withdrawTest() {
        account.withdraw(500);
        assertEquals(account.getBalance(),4500.0,delta,"Withdraw Test");
    }

    @Test
    void interestTest() {
        double interest = account.calculateInterest();
        assertEquals(interest,750.0,delta,"Interest Test");
    }

}