package qa.java.lab.files;

import java.io.*;

public class LibraryFileEditor {
	
  public static final String fileName = "C:\\qajavnc1\\Labs\\Files_and_Exceptions\\files\\members.txt";
	
	// REMEMBER TO DELETE COMMENTS AS YOU GO ALONG!
	// REMEMBER TO DELETE COMMENTS AS YOU GO ALONG!
	// REMEMBER TO DELETE COMMENTS AS YOU GO ALONG!
	// This practical is different to the others coded so far
	// You will be instructed when to compile, as we want you to see 
	// certain compilation errors and how to remove them
		
  public static void main(String[] args) {

	// QA TODO Step 1:
        // Make 2 method calls from 'main'
	// Call the inputMember() method which will when coded will
	// ask for a new member.
	// Then call the showMembers() method which will when coded 
	// display the contents of the file. 
	// COMPILE.
	// End of Step 1. Delete this comment and scroll down for Step 2.


        // QA To Do Step 5: 
	// Now we are in main so the 'buck stops here'
        // Introduce try{} and catch(){}
	// Wrap the 2 method calls in main in a try {} block and catch the IOException 
	// with a block that does nothing e.g {}
	// Ensure the code COMPILEs
	// Having seen that you are not actually forced to do anything
	// lets display  "Problem with file handling." to System.err
	// Now run the application supplying a name and a numeric age
	// End of Step 5. Step 6 immediately follows
		

	// QA To Do Step 6: 
        // Handling the non numeric data
	// Run the application again this time supplying a non numeric age
	// Result - The JVM crashed with a stack trace due to 
	// the NumberFormatException thrown by Integer.parseInt()
	// Note that     a) you did not 'try' Integer.parseInt()
	//  nor did you  b) 'pass the buck' 
	//                   by coding 'throws NumberFormatException'
	// NumberFormatException is an unchecked runtime exception
	// that the compiler will ignore but we ought to catch it here
	// Code a catch block now, here, at this point that prints 
	// to System.err       e.getMessage() + " is an invalid age"


	// COMPILE and test again with non numeric age and
	// see the message including duff age printed in red (System.err)
	// End of Step 6, scroll down to Step 7 in showMembers()
		
	// QA To Do Step 8: 
        // Testing File not found
	// The showMembers() method potentially throws either an 
	// IOException or a FileNotFoundException 

	// Code at this point here, now at this location a catch of 
	// FileNotFoundException displaying a suitable 'Not Found' 
	// message to System.err

	// Try and COMPILE, it won't as 'code not reachable'
	// Cut/paste the catch immediately before the more generic 
	// catch of IOException up above
	// Then test the app after making a temporary typo in the filename
	// at top of class
	// End of Step 8, Scroll to the bottom of the class for Step 9
	
	// QA To Do Step 11: 
        // Catch UnderAgeException and produce a display to System.err
	// of a message displaying the invalid age that is encapsulated 
	// in the exception plus the contents of the string passed 
	// into the constructor when it was thrown from inputMember()
	// COMPILE
	// Test now supplying an under age member
	// Check this works.
        // End of step 11, Scroll to Step 12 (at bottom of showMembers())
				
  }   // end of main()

  public static void inputMember() {
    String name, strAge;
    int age;
	
	// QA TODO Step 2: 
        // Calling getInput() twice 
	// Call the getInput() method, asking for a name,
	// storing the return value in the variable 'name'.
	// Call the getInput() method, asking for an age,
	// storing the return value in the variable 'strAge' 


	// Convert the 'strAge' to an int using class method 'parseInt'
	// of class 'Integer' and store in the variable called 'age'
	// don't worry about any non-numeric data at this stage
	// COMPILE. 
        // End of Step2, Delete comment and scroll down to find Step 3
	// before coming back here for step 4.

        // QA To Do Step 4: 
        // Pass the buck again
	// This method does not compile now as you are calling a method 
	// that throws but does not catch an IOException
	// You must catch it or pass the buck!
	// Pass the buck to main()
        // Try and COMPILE, inputMember() will compile but main() won't!
        // End of Step 4, Scroll up to Step 5

	// QA To Do Step 10: 
        // Deal with age less than 16 problem
	// If the 'age' is less than 16 then create and throw an instance 
	// of UnderAgeException passing the invalid age into the constructor


	// COMPILE. It fails because the exception extends Exception but does not
	// extend RunTimeException meaning it is a checked exception that must
	// be handled somewhere, so lets pass the buck up to main
	// Change the signature of this method (inputMember) to also 
        // 'throws' UnderAgeException
	// COMPILE
	// End of Step 10, Scroll up to Step 11 in main to add the catch
	
  }	// end of inputMember ()
	
        

  public static String getInput(String question) {
		
    String keyboardinput = null;

	// QA To Do Step 3: 
        // In this step you will return keyboard data keyed in from console
	// Print a message to the console asking the 'question'


	// Declare and create an InputStreamReader using 'System.in' 
	// as its constructor argument


	// Declare and create a BufferedReader using your InputStreamReader 
	// reference as its constructor argument


	// Use the BufferedReader's readLine() to retrieve your keyboard input
	// That is the string the method should return

    return keyboardinput;

	// Try and COMPILE, it will fail. 
	// The IOException needs to be handled or the buck passed to the calling method

	// Pass the buck!, declare the method as throwing IOException
	// Try and COMPILE, getInput() will compile but the 2 calls to it
	// from inputMember() won't
 
	// End of Step 3, Scroll up to Step 4 to revisit inputMember().
		
  }	// end of getInput()
	
  
  public static void showMembers() {

    String data;
		
	// QA To Do Step 7: 
        // We will now read data from supplied .txt file
	// Firstly declare and create a FileInputStream using the
	// presupplied instance variable 'fileName' as a constructor argument 


	// Declare and create a InputStreamReader using your FileInputStream
	// reference as its constructor argument 


	// Declare and create a BufferedReader using your InputStreamReader 
	// reference as its constructor argument, call it 'bfr'


	// Read one line of the file into the 'data' variable


	// While 'data' is not null (readLine() eventually returns null)
	//		- print it out to console and
	//      - read the next line into 'data'

	// Call close() on the BufferedReader when loop exits, don't worry 
	// about putting it in a finally clause yet, that comes later

	// COMPILE the code, several errors appear referring 
	// to 2 different exception types 
	// Lets 'pass the buck'. You only need to 'throws IOException'
	// as it is the superclass of FileNotFoundException
	// The code now compiles and runs and displays the members
	// End of Step 7, Scroll up to Step 8 in main

	// QA To Do Step 12:
        // Introduce a finally {}
	// We have not ensured that the BufferedReader is closed
	// Place bfr.close() in a finally clause


	// Wrap all the rest of the method in a try {} block
	// COMPILE
	// It fails because 'bfr' is declared in and is local to the try!
	// Ensure that the BufferedReader is declared (just declared) prior to the 
	// 'try', you will also be asked to ensure that the 
	// reference is initialised (to null) prior to the 'try' block.
	// COMPILE
	// Put a System.out.println("We're in finally"); as 1st statement 
	// in the finally and ensure it appears whether the file 
	// is found or not.
	// You will find it does, but your code will crash in the finally
	// You can sort that one out yourself!!
	// If you get this all working go to the very bottom of the file
	// to find some very optional extra tasks 
        // End of step 12

  }	// end of showMembers()

}		// end of class

        // QA To Do Step 9: 
        // Write yor own Exception class
	// Call it UnderAgeException 
	// It should
	// a) be package visible - no choice at this location
	// b) extend 'Exception'

	// c) have a private int age variable

	// d) have a public int getAge() method

	// e) have a constructor that simply receives an int and does 2 things
	//    i) passes the String "Too young for Library" to
	//        its superclass constructor
	//    ii) stores the int argument in instance variable 'age'
	// COMPILE
        // End of Step 9. Scroll up to Step 10 in the middle of inputMember() 
	

// QA To Do Very Optional steps
// 1)   Make the UnderAgeException an inner class
//      Just cut it out and drop it above the close brace } of the main class
//	Did you get it working? the compilation error is slightly misleading
//      Think about what does an instance of an inner class need?

// 2)	Put the whole of main in a while loop
//      so that they can potentially enter lots of members
//      keeping prompting them "Type 'quit' to exit" and keep 
//      looping until they type in 'quit'. But allow them to type 
//      quit in upper case, lower case, mixed case etc
//      String reply = getInput("Type 'quit' if you want to exit");	
//	  would be a good 1st line of main before the while(....)

